<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Prova de API</title>
        <!-- Latest compiled and minified CSS -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
  <table class="table">
      <caption>Tarefas cadastradas.</caption>
      <thead>
        <tr>
          <th>#</th>
          <th>TITULO</th>
          <th>DESCRIÇÃO</th>
          <th>PRIORIDADE</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($tarefas as $tarefa) { ?>
        <tr>
          <th scope="row"><?php echo $tarefa->id ?></th>
          <td><?php echo $tarefa->titulo ?></td>
          <td><?php echo $tarefa->descricao ?></td>
          <td><?php echo $tarefa->prioridade ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
</div>

</body>
</html>