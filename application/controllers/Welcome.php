<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	
	public function index()
	{   
                $data['tarefas'] = $this->getAll();
		$this->load->view('welcome_message',$data);
	}
        
        function add()
        {
            $titulo = $this->input->post('titulo');
            $descricao = $this->input->post('descricao');
            $prioridade = $this->input->post('prioridade');
            $tarefa = array('titulo' => $titulo, 'descricao' => $descricao, 'prioridade' => $prioridade);
            $this->db->insert('tarefas', $tarefa);
            echo "Inserido com sucesso: " . json_encode($tarefa);
        }
        
        function update($id)
        {
            $titulo = $this->input->post('titulo');
            $descricao = $this->input->post('descricao');
            $prioridade = $this->input->post('prioridade');
            $tarefa = array('titulo' => $titulo, 'descricao' => $descricao, 'prioridade' => $prioridade);
            $this->db->where('id',$id);
            $this->db->update('tarefas', $tarefa);
            echo "Atualizado com sucesso: " . json_encode($tarefa);
        }
        
        function delete($id)
        {
            $this->db->where('id',$id);
            $this->db->delete('tarefas', $this);
            echo "Removido com sucesso: id " . json_encode($id);
        }

        public function getAll()
        {
            $this->db->order_by("prioridade", "desc"); 
            return $this->db->get('tarefas')->result();
        }
}
